import re


def checkIt(mas):
    for i in range(1, len(mas)):
        if len(mas[i]) > 4 or len(mas[i]) < 2:
            return False
    return True


f = open("input.txt")
a = f.read()
print(checkIt(list(re.split('(?=[A-Z])', a))))
